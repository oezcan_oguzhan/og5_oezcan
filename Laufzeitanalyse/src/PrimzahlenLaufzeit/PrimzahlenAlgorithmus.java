package PrimzahlenLaufzeit;



public class PrimzahlenAlgorithmus {
	
	public static boolean isPrim(long zahl) {
		long zaehler = 0;
		for (long i =  zahl; i >= 1; i--) {
			if (zahl % i == 0) {
				zaehler++;
			}
		}
		if (zaehler > 2 || zahl == 1)
			return false;
		else
			return true;
	}

}
