package PrimzahlenLaufzeit;

//Author: Oguzhan �zcan
public class Stoppuhr {

	private  long startpunkt;
	private  long endpunkt;
	
	public void starten(){
		startpunkt = System.currentTimeMillis();
	}
	
	public void stoppen(){
		endpunkt = System.currentTimeMillis();
	}
	
	public double getDauer(){
		double dauer = (double)(endpunkt - startpunkt)/1000;
		return dauer;
	}
	
	public void reset(){
		startpunkt = 0;
		endpunkt = 0;
	}
}