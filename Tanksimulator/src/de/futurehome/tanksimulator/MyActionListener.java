package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {				//F�llt 5.0 auf bei jedem Klick auf den Button "Einf�llen"
			 double fuellstand = f.myTank.getFuellstand();
			 double differenz = 0.0;
			 if(fuellstand<=100){
				 fuellstand = fuellstand + 5; 
			 }
			 
			 if(fuellstand>100){      //F�llstand wird limitiert auf den maximalen Wert von 100
				 differenz = fuellstand - 100;
				 fuellstand=fuellstand - differenz;
			 }
			 
			 f.myTank.setFuellstand(fuellstand);

			 f.lblFuellstand.setText(""+fuellstand+"  "+fuellstand+"%");  // 1l = 1%
		}

		
		if(obj == f.btnVerbrauchen){
		    double fuellstand = f.myTank.getFuellstand();
		    double verbrauch = 2.0;
		    double differenz = 0.0;
		    if(fuellstand>=0){
		    	fuellstand = fuellstand - verbrauch;
		    }
		    if(fuellstand<0){									//F�llstand wird limitiert auf den minimalen Wert von 100
		    	differenz=fuellstand;
		    	differenz=differenz*(-1);
		    	fuellstand=fuellstand+differenz;
		    }
		    	
		    	f.myTank.setFuellstand(fuellstand);
		    	f.lblFuellstand.setText(""+fuellstand+"  "+fuellstand+"%");
		    }
		
		
		
		if(obj == f.btnZuruecksetzen){					//setzt den Tank wieder auf 0		
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText(""+fuellstand);
		}
		
		
	}
}