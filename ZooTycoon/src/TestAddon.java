import java.util.Scanner;

public class TestAddon {
    public static void main (String [] args){
    	Addon Tierfutter = new Addon("1234", "Bananen", 5.99, 20, 3);
    	
    	System.out.println("ID-Nummer: "+Tierfutter.getIdNummer());
    	System.out.println("Bezeichnung: "+Tierfutter.getBezeichung());
    	System.out.println("Verkaufspreis: "+Tierfutter.getVerkaufspreis()+" �");
    	System.out.println("Maximaler Bestand: "+Tierfutter.getMaxBestand());
    	System.out.println("Aktueller Bestand: "+Tierfutter.getAktuellerBestand());
    
    	System.out.println("Geben Sie den Wert ein um den der Bestand ver�ndert werden soll: ");
    	Scanner scan= new Scanner(System.in);
    	int wert = scan.nextInt();
    	
    	Tierfutter.aendereBestand(wert);
    	
    	System.out.println("Neuer Bestand: "+Tierfutter.getAktuellerBestand());
    	System.out.println("Gesamtwert des Bestandes: "+Tierfutter.berechneGesamtwert()+" �");
    }
}
