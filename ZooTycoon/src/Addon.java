
public class Addon {
		
	public Addon(){
			this.idNummer=" ";
			this.Bezeichung=" ";
			this.verkaufspreis=0.0;
			this.maxBestand=0;
			this.aktuellerBestand=0;	
		}
	
public Addon(String idNummer, String Bezeichnung, double verkaufspreis, int maxBestand, int aktuellerBestand){
		this.idNummer=idNummer;
		this.Bezeichung=Bezeichnung;
		this.verkaufspreis=verkaufspreis;
		this.maxBestand=maxBestand;
		this.aktuellerBestand=aktuellerBestand;
}
	
	private String idNummer;
		public String getIdNummer() {
			return idNummer;
		}
		
		public void aendereBestand(int wert){ //mit limits auf maximalen und minimalen Wert
			int differenz=0;
			if (wert>=0){
				this.aktuellerBestand=this.aktuellerBestand+wert;
			}else{
				this.aktuellerBestand=this.aktuellerBestand+wert;
			}
			
			if(this.aktuellerBestand>this.maxBestand){
			differenz = aktuellerBestand-maxBestand;
			aktuellerBestand=aktuellerBestand-differenz;
			}
			
			
			if(this.aktuellerBestand<0){
				
				aktuellerBestand=0;
				
				
			}
			
			
			
		}
		
		public double berechneGesamtwert(){
			
			return this.verkaufspreis*this.aktuellerBestand;
		}
		
		public void setIdNummer(String idNummer) {
			this.idNummer = idNummer;
		}
		
		public String getBezeichung() {
			return Bezeichung;
		}
		
		public void setBezeichung(String bezeichung) {
			Bezeichung = bezeichung;
		}
		
		public double getVerkaufspreis() {
			return verkaufspreis;
		}
		
		public void setVerkaufspreis(double verkaufspreis) {
			this.verkaufspreis = verkaufspreis;
		}
		
		public int getMaxBestand() {
			return maxBestand;
		}
		
		public void setMaxBestand(int maxBestand) {
			this.maxBestand = maxBestand;
		}
		
		public int getAktuellerBestand() {
			return aktuellerBestand;
		}
		
		public void setAktuellerBestand(int aktuellerBestand) {
			this.aktuellerBestand = aktuellerBestand;
		}
		
		private String Bezeichung;
		private double verkaufspreis;
		private int maxBestand;
		private int aktuellerBestand;
		
}
