
public class KeyStore01 {
	private String[] s;
	private int length;

	public KeyStore01() {
		s = new String[100];
		this.length = 100;
	}

	public KeyStore01(int length) {
		s = new String[length];
		this.length = length;
	}

	public boolean add(String eintrag) {
		int index = 0;
		for (int i = 0; i < this.length - 1; i++) {
			if (s[i] == null) {
				s[i] = eintrag;
				index = i;
				break;
			}
		}

		if ((index - 1) >= this.length - 1)
			return false;
		else
			return true;
	}

	public int indexOf(String s) {
		for (int i = 0; i < this.length - 1; i++) {
			if (s.equals(this.s[i])) {
				return i;
			}
		}
		return -1;
	}

	public void remove(int i) {
		if (i >= 0)
			this.s[i] = null;

	}
}
