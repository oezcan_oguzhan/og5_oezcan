
//author Oguzhan

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class FileControl {
	public void dateiEinlesenUndAusgeben(File file) throws IOException {

		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String s = br.readLine();
		List<Lerneintrag> eintrag = new ArrayList<Lerneintrag>();
		String name = br.readLine();
		while (s != null) {

			Date datum = Date.valueOf(br.readLine());
			String fach = br.readLine();
			String beschreibung = br.readLine();
			int dauer = Integer.parseInt(br.readLine());

			Lerneintrag einEintrag = new Lerneintrag(datum, fach, beschreibung, dauer);
			eintrag.add(einEintrag);
		}
	}

	public void dateiEinlesen(File file) {

	}

	public static void main(String[] args) throws IOException {

		FileControl myFC = new FileControl();
		myFC.dateiEinlesenUndAusgeben(new File("miriam.dat"));

	}

}
