import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Hauptfenster extends JFrame {

	
	public Hauptfenster(){
	// FRAME ZENTRIEREN
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			int x = (int) ((dimension.getWidth() - 780) / 2);
		
			
			//F�r die Optik
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			
	setResizable(true);
	setVisible(true);
	setTitle("SofaLearning Lerntagebuch");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(x, 210, 710, 300);
	JPanel contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(10, 15, 5, 15));
	contentPane.setLayout(new BorderLayout()); // <-- Layout
	setContentPane(contentPane);
	
	JLabel lbl_name = new JLabel("Lerntagebuch von Miriam");
	contentPane.add(lbl_name, BorderLayout.NORTH);
	lbl_name.setFont(new Font("Tahoma", Font.PLAIN, 20));
	
	
	
	
	
	TextArea text_area = new TextArea("",1,1,TextArea.SCROLLBARS_VERTICAL_ONLY);
	contentPane.add(text_area, BorderLayout.CENTER);
	
	text_area.setEditable(false);
	
	
	
	JPanel pnl_btn = new JPanel();
	contentPane.add(pnl_btn, BorderLayout.SOUTH);
	pnl_btn.setLayout(new FlowLayout(FlowLayout.RIGHT));
	
	JButton btn_1 = new JButton("Neuer Eintrag");
	pnl_btn.add(btn_1);
	
	JButton btn_2 = new JButton("Bericht...");
	pnl_btn.add(btn_2);
	
	JButton btn_3 = new JButton("Beenden");
	pnl_btn.add(btn_3);
	
	
	
	
	btn_3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	});
	pnl_btn.add(btn_3);
	
	
	}
	public static void main ( String [] args){
		new Hauptfenster();
	}
}
