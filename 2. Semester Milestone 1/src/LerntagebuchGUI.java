
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Antonio 
 * 14.01.2017
 * 
 */

@SuppressWarnings("serial")
public class LerntagebuchGUI extends JFrame {

	static File file = new File("antonio.dat");

	public LerntagebuchGUI() throws IOException {

		// FRAME ZENTRIEREN
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - 780) / 2);

		// OBJEKTE
//		FileControl el = new FileControl();
//		el.dateiEinlesenUndAusgeben(file);

		// LOOK AND FEEL (WINDOWS)
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// GUI
		setResizable(false);
		setTitle("SofaLearning Lerntagebuch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(x, 210, 708, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 15, 5, 15));
		contentPane.setLayout(new BorderLayout()); // <-- Layout
		setContentPane(contentPane);

		JLabel lblLearnerName = new JLabel("Lerntagebuch von Miriam");
		lblLearnerName.setHorizontalAlignment(SwingConstants.LEFT);
		lblLearnerName.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblLearnerName, BorderLayout.NORTH);

		JPanel pnlCenterInfos = new JPanel();
		pnlCenterInfos.setBorder(new EmptyBorder(10, 0, 5, 0));
		contentPane.add(pnlCenterInfos, BorderLayout.CENTER);
		pnlCenterInfos.setLayout(new GridLayout());// <-- Layout

		TextArea ausgabe = new TextArea("", 1, 1, TextArea.SCROLLBARS_VERTICAL_ONLY);
//		ausgabe.setText(el.dateiEinlesenUndAusgeben(file));
		ausgabe.setFont(new Font("monospaced", Font.BOLD, 14));
		ausgabe.setEditable(false);
		pnlCenterInfos.add(ausgabe);

		JPanel pnlButtons = new JPanel();
		contentPane.add(pnlButtons, BorderLayout.SOUTH);
		pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));// <-- Layout

		JButton btnEintrag = new JButton("Neuer Eintrag...");
		pnlButtons.add(btnEintrag);

		JButton btnEintraga = new JButton("Bericht...");
		pnlButtons.add(btnEintraga);

		JButton btnEintragd = new JButton("Beenden");
		btnEintragd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnEintragd);

	}

	public static void main(String args[]) throws IOException{
			new LerntagebuchGUI().setVisible(true);

	}

}
