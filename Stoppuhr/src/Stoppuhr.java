//Author: Oguzhan �zcan
public class Stoppuhr {
	private long startpunkt;
	private long endpunkt;
	
	public Stoppuhr(){
		this.startpunkt=0;
		this.endpunkt=0;
	}
	
	public void starten(){
		this.startpunkt= System.currentTimeMillis();
	}
	
	public void stoppen(){
		this.endpunkt=this.startpunkt;
	}
	
	public void reset(){
		this.startpunkt=0;
		this.endpunkt=0;
	}
	
	public long getDauer(){
		this.endpunkt=this.endpunkt-this.startpunkt;
		return this.endpunkt;
	}
	
}
