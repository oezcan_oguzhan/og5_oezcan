public class Quicksort {
	public static void sortiere(int x[]) {
		qSort(x, 0, x.length - 1);
	}

	public static void qSort(int x[], int links, int rechts) {
		if (links < rechts) {
			int i = partition(x, links, rechts);
			qSort(x, links, i - 1);
			qSort(x, i + 1, rechts);
		}
	}

	public static int partition(int x[], int links, int rechts) {
		String ausgabe = "";

		int pivot, i, j, help;
		pivot = x[rechts];

		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (x[i] > pivot) {
				// tausche x[i] und x[j]

				help = x[i];
				x[i] = x[j];
				x[j] = help;
				j--;

			} else
				i++;
			for (int h = 0; h < x.length; h++) {
				ausgabe = ausgabe + x[h] + " ";
			}
			System.out.println(ausgabe + "    Privot Element: " + pivot);
			ausgabe = "";

		}
		System.out.println(" ");
		// tausche x[i] und x[rechts]
		help = x[i];
		x[i] = x[rechts];
		x[rechts] = help;
		System.out.println("tausche x[i] und x[rechts]");
		System.out.println(" ");
		for (int h = 0; h < x.length; h++) {
			ausgabe = ausgabe + x[h] + " ";
		}
		System.out.println(ausgabe + "    Privot Element: " + pivot);
		ausgabe = "";
		return i;
	}

	public static void main(String[] args) {
		int[] liste = { 1, 9, 0, 6, 4, 8, 5, 3, 7, 2 };

		Quicksort.sortiere(liste);

	}
}
