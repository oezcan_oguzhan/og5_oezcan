
public class Selectionsort {
	public static void main(String[] args) {

		int[] unsortiert = { 1, 9, 0, 6, 4, 8, 5, 3, 7, 2};
		String ausgabe ="";
		for (int h = 0; h < unsortiert.length; h++) {
			ausgabe = ausgabe + unsortiert[h] + " ";
		}
		System.out.println(ausgabe+" <-- unsortiert");
		ausgabe="";
		
		
		int[] sortiert = selectionsort(unsortiert);

		for (int h = 0; h < sortiert.length; h++) {
			ausgabe = ausgabe + sortiert[h] + " ";
		}
		System.out.println(ausgabe+" <-- sortiert");
		ausgabe="";

	}

	public static int[] selectionsort(int[] sortieren) {
		String ausgabe ="";
		
		for (int i = 0; i < sortieren.length - 1; i++) {
			for (int j = i + 1; j < sortieren.length; j++) {
				if (sortieren[i] > sortieren[j]) {
					
					int temp = sortieren[i];
					sortieren[i] = sortieren[j];
					sortieren[j] = temp;
					for (int h = 0; h < sortieren.length; h++) {
						ausgabe = ausgabe + sortieren[h] + " ";
					}
					System.out.println(ausgabe);
					ausgabe="";
				}
			}
		}

		return sortieren;
	}
}
