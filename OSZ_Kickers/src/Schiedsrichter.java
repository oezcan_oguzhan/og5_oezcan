
public class Schiedsrichter extends Person {
	
	private int anzahlGepfiffeneSpiele ;

	public Schiedsrichter(String name, String telefonnummer, boolean jahresbeitragbezahlt, int anzahlSpiele) {
		super(name, telefonnummer, jahresbeitragbezahlt);
		this.anzahlGepfiffeneSpiele = anzahlSpiele;
	}

	public int getAnzahlSpiele() {
		return anzahlGepfiffeneSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlGepfiffeneSpiele = anzahlSpiele;
	}
	

}
