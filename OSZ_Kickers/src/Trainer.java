
public class Trainer extends Person {

	private char lizenzklasse;
	private int aufwandsentschaedigung;
	
	
	public Trainer(String name, String telefonnummer, boolean jahresbeitragbezahlt, char lizenzklasse,int monatlicheAufwandentschaedigung) {
		super(name, telefonnummer, jahresbeitragbezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsentschaedigung = monatlicheAufwandentschaedigung;
	}
	
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public int getMonatlicheAufwandentschaedigung() {
		return aufwandsentschaedigung;
	}
	public void setMonatlicheAufwandentschaedigung(int monatlicheAufwandentschädigung) {
		this.aufwandsentschaedigung = monatlicheAufwandentschädigung;
	}
	
	
	
}
