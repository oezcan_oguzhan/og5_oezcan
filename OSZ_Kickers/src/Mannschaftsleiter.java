
public class Mannschaftsleiter extends Spieler {

	private String nameMannschaft;
	private int rabattJahresbeitrag;
	
	public Mannschaftsleiter(String name, String telefonnummer, boolean jahresbeitragbezahlt, int trikotnummer,
			String spielposition, String nameMannschaft, int rabattaufjahresbetrag) {
		super(name, telefonnummer, jahresbeitragbezahlt, trikotnummer, spielposition);
		this.nameMannschaft = nameMannschaft;
		this.rabattJahresbeitrag = rabattaufjahresbetrag;
	}
	
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getRabattaufjahresbetrag() {
		return rabattJahresbeitrag;
	}
	public void setRabattaufjahresbetrag(int rabattaufjahresbetrag) {
		this.rabattJahresbeitrag = rabattaufjahresbetrag;
	}
}
