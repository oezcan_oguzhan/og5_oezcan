
public class TestKickers {

	public static void main(String[] args) {

		
		System.out.println("Klasse Spieler");
		Spieler manuel = new Spieler("Manuel", "+491231412", false, 24, "Mitte");
		
		

		System.out.println("Name: " + manuel.getName());
		System.out.println("Telefonnummer: " + manuel.getTelefonnummer());
		System.out.println("Jahresbeitrag gezahlt?: " + manuel.isjahresbeitragbezahlt());
		System.out.println("Trikotnummer: " + manuel.getTrikotnummer());
		System.out.println("Spielposition: " + manuel.getSpielposition());
		
		
		System.out.println("");
		System.out.println("Klasse Trainer");
		
		
		
		Trainer oezcan = new Trainer("Oezcan", "+4917632523", false, 'B', 165);

		System.out.println("Name: " + oezcan.getName());
		System.out.println("Telefonnummer: " + oezcan.getTelefonnummer());
		System.out.println("Jahresbeitrag gezahlt?: " + oezcan.isjahresbeitragbezahlt());
		System.out.println("Lizenzklasse:  " + oezcan.getLizenzklasse());
		System.out.println("Aufwandsentsch�digung: " + oezcan.getMonatlicheAufwandentschaedigung());

		
		System.out.println("");
		System.out.println("Klasse Schiedsrichter");
		
		
		Schiedsrichter ginger = new Schiedsrichter("Kool Savas", "+49597616531", true, 15);

		System.out.println("Name: " + ginger.getName());
		System.out.println("Telefonnummer: " + ginger.getTelefonnummer());
		System.out.println("Jahresbeitrag gezahlt?: " + ginger.isjahresbeitragbezahlt());
		System.out.println("Anzahl gepfiffener Spiele: " + ginger.getAnzahlSpiele());

		
		Mannschaftsleiter hamudi = new Mannschaftsleiter("Hamudi", "+5654656465", true, 18, "Links", "Menschester Yunaitet",50);
		
		
		System.out.println("");	
		System.out.println("Klasse Name");

		
		System.out.println("Name: " + hamudi.getName());
		System.out.println("Telefonnummer: " + hamudi.getTelefonnummer());
		System.out.println("Jahresbeitrag gezahlt?: " + hamudi.isjahresbeitragbezahlt());
		System.out.println("Trikotnummer: " + hamudi.getTrikotnummer());
		System.out.println("Spielposition: " + hamudi.getSpielposition());
		System.out.println("Mannschaftsname: "+ hamudi.getNameMannschaft());
		System.out.println("Rabatt auf Jahresbeitrag: "+ hamudi.getRabattaufjahresbetrag() +" %");
	
	}
}
