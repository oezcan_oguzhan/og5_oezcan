package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (Oguzhan �zcan)
 * @version (v01 26.09.2016)
 */
public class Ladung {

	// Attribute
	private int posX;
	

	private int posY;
	private int masse;
	private String typ;
	// Methoden
	
	public Ladung(){
		this.posX=0;
		this.posY=0;
		this.masse=0;
		this.typ="Unbekannt";
	}
	
	public Ladung(int posX,int posY, int masse, String typ){
		this.posX=posX;
		this.posY=posY;
		this.masse=masse;
		this.typ=typ;
	}
	
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}