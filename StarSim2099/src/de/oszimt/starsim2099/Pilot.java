package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (Oguzhan �zcan)
 * @version (v01 26.09.2016)
 */
public class Pilot {

	// Attribute
	private int posX;
	private int posY;
	private String grad;
	private String name;
	
	
	// Methoden
	public Pilot(){
		this.posX=0;
		this.posY=0;
		this.grad="Unbekannt";
		this.name="Unbekannt";
	}
	
	public Pilot(int posX,int posY, String grad, String name){
		this.posX=posX;
		this.posY=posY;
		this.grad=grad;
		this.name=name;
	}
	
	
	
	
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
