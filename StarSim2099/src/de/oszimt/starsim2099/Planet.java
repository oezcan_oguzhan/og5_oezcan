package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (Oguzhan �zcan)
 * @version (v01 26.09.2016)
 */
public class Planet {

	// Attribute
	private int posX;
	private int posY;
	private int anzahlHafen;
	private String name;
	// Methoden

	public Planet(){
		this.posX=0;
		this.posY=0;
		this.anzahlHafen=0;
		this.name="Unbekannt";
	}
	
	public Planet(int posX,int posY, int anzahlHafen, String name){
		this.posX=posX;
		this.posY=posY;
		this.anzahlHafen=anzahlHafen;
		this.name=name;
	}
	


	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
	}



	public int getPosY() {
		return posY;
	}



	public void setPosY(int posY) {
		this.posY = posY;
	}


	public int getAnzahlHafen() {
		return anzahlHafen;
	}



	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}


	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}


	
	
	
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

}

}





